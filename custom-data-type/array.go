package customdatatype

type number interface {
	int | int8 | int16 | int32 | int64 |
		float32 | float64
}

type Array[T number] struct {
	Value      []T     `json:"value"`
	Sets       []T     `json:"set"`
	LinkedList List[T] `json:"linked_list"`
}

func NewArray[T number](key []T) *Array[T] {
	return &Array[T]{
		Value: key,
	}
}

func (a *Array[T]) Init() {

	// create hashmap and set
	hashMap := map[T]bool{}
	set := []T{}
	list := &List[T]{}
	for _, v := range a.Value {
		if !hashMap[v] {
			hashMap[v] = true
			set = append(set, v)
		}
		list.Insert(v)
	}
	a.Sets = set
	a.LinkedList = *list
	a.LinkedList.Reverse()
}

func (a *Array[T]) InsertBefore(k, f T) {

	s := a.LinkedList.Head

	node := &Node[T]{
		Key: k,
	}

	if s.Key == f {
		node.Next = a.LinkedList.Head
		a.LinkedList.Head.Prev = node
		a.LinkedList.Head = node
	} else {
		for {
			if s.Next.Key == f {
				break
			}
			s = s.Next
			if s.Next == nil {
				break
			}
		}

		if s.Next == nil {
			s.Next = node
			node.Prev = s
			a.LinkedList.Tail = node
		} else {
			next := s.Next
			s.Next = node
			node.Prev = s
			node.Next = next
			next.Prev = node
		}

	}

	// update value
	i := a.LinkedList.Head
	arr := []T{}
	for {
		if i == nil {
			break
		}
		arr = append(arr, i.Key)
		i = i.Next
	}
	a.Value = arr
	a.addSets(k)
}

func (a *Array[T]) addSets(k T) {
	find := false
	for _, v := range a.Sets {
		if v == k {
			find = true
		}
	}
	if !find {
		a.Sets = append(a.Sets, k)
	}
}
