package customdatatype

import "fmt"

type Node[T number] struct {
	Key  T
	Next *Node[T]
	Prev *Node[T]
}

type List[T number] struct {
	Head *Node[T]
	Tail *Node[T]
}

func (l *List[T]) Insert(key T) {
	node := &Node[T]{
		Key:  key,
		Next: l.Head,
	}
	if l.Head != nil {
		l.Head.Prev = node
	}
	l.Head = node

	t := l.Head
	for t.Next != nil {
		t = t.Next
	}
	l.Tail = t
}

func (n *Node[T]) Display() {
	for n != nil {
		fmt.Printf("-> %v", n.Key)
		n = n.Next
	}
	fmt.Println()
}

func (n *Node[T]) Showbackward() {
	for n != nil {
		fmt.Printf("-> %v", n.Key)
		n = n.Prev
	}
	fmt.Println()
}

func (l *List[T]) Reverse() {
	curr := l.Head
	var prev *Node[T]
	l.Tail = l.Head

	for curr != nil {
		next := curr.Next
		curr.Next = prev
		prev = curr
		curr = next
	}
	l.Head = prev
}
