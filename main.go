package main

import (
	customdatatype "data-structure/custom-data-type"
	"fmt"
)

func main() {

	// linked list example
	arr := []int{7, 7, 8, 0, 8, 3, 0, 1}

	customArray := customdatatype.NewArray(arr)

	customArray.Init()

	fmt.Println(customArray.Value)

	customArray.InsertBefore(5, 1)

	fmt.Println(customArray.Value)
	fmt.Println(customArray.Sets)

	customArray.InsertBefore(15, 8)

	fmt.Println(customArray.Value)
	fmt.Println(customArray.Sets)

}
